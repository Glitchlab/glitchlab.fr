# Glitchlab.fr

Source et contenu du site [Glitchlab.fr](https://glitchlab.fr).



## Glitcher

Vous pouvez librement participer et contribuer à l'évolution de ce site en éditant ou ajoutant un fichier depuis [le dépôt public de Glitchlab.fr](https://framagit.org/Glitchlab/glitchlab.fr)

### Éditer du contenu

Pas besoins d'être journaliste ou écrivain pour participer au contenu du site, vous avez vu une coquille voulez ajouter une photos ou améliorer une tournure de phrase; vous êtes libre d'éditer ou d'[ajouter votre participation](https://framagit.org/Glitchlab/glitchlab.fr/issues/new).

Des [connaissances de base en HTML](https://developer.mozilla.org/fr/docs/Web/HTML) seront un plus, mais pas nécessaire. Vous pouvez ajouter votre contenu sans marquage HTML, des participants plus expirmentés en HTML et CSS pourrons vous aider.

### Le contenu graphique

Pour l'ajout d'images, respectons les serveurs communautaires et l'environnement en évitant de télèverser des images trop lourdes; il n'est pas nécessaires de télèverser des images trop lourdes. Veillez à respecter cette liste de recommandations :

#### Les formats
Pour **les photos**, utilisez le format **[JPG](https://fr.wikipedia.org/wiki/JPEG)** ou **[PNG](https://fr.wikipedia.org/wiki/Portable_Network_Graphics) 24bits**.

Pour **les illustrations** et les images comportant **peu de détails**, un format **[GIF](https://fr.wikipedia.org/wiki/Graphics_Interchange_Format)** ou **[PNG](https://fr.wikipedia.org/wiki/Portable_Network_Graphics)** 8its.

Pour les **illustrations vectorielles**, il faudrat choisir le format **[SVG](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics)**.

Veillez à toujours optimiser vos images avant de le télèverser (via [ImageOptim](https://github.com/ImageOptim/ImageOptim), [SVGO](https://github.com/svg/svgo) ou [compressor.io](https://compressor.io/). Choisissez une compression entre 60 et 80 pour le format JPG et a table de couleur la plus réduite pour les GIT et PNG 8 bits.

#### Les dimensions et le nommage

Il est inutile de télèverser des images d'une dimenssion supérierur à celles utilisés par le site, voici la liste des dimensions en pixels à utiliser :

| Visuel de couverture  | |
|------------|------------|
| original   | 1920 × 822 |
| grand      | 1440 × 617 |
| moyen      | 1280 × 640 |
| petit      | 1024 × 576 |
| très petit | 640 × 427  |
| vignette   | 320 × 240  |

Notez que les dimension de largeur doivent toujours être respectées, celle de hauteurs sont un maximum; par exemple vous pouvez avoir une image d'une dimension de 1920 × 720 par exemple, mais pas 1920 × 900.

glitchlab-couverture_1920x822.png


**Visuel de contenu**

glitchlab-contenu_320x240.png
glitchlab-contenu_640x427.png
glitchlab-contenu_800x450.png

### Éditer du code

Apprendre, comprendre, faire; peut importe votre niveau en développement web, n'hésitez pas à participer à l'amélioration du source.

Le site et volontairement simple, à la sauce [KISS](https://fr.wikipedia.org/wiki/Principe_KISS) (le principe, pas le groupe :P); nous appliquerosn également la philosophie [DRY](https://fr.wikipedia.org/wiki/Ne_vous_r%C3%A9p%C3%A9tez_pas).

Le choix d'un HTML et CSS [vanilla](https://en.wikipedia.org/wiki/Plain_vanilla) n'est pas innocent, il permettras au débutant de participer et aux experts de s'amuser à pousser leurs connaissances HTML et CSS dans leurs retranchement.

Pour une collaboration harmonieuse, il est obligatoire de suivre les conventions et lignes de conduites suivantes :

 - http://codeguide.co/
